CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The Views Negate module provides a "Not Equal" to condition
for list_string values in contextual filter.
It works same as exclude option of numeric type field.

A checkbox with "Negate" label is added under "MORE" fieldset.
By selecing the Negate option, it applies the "not equal" condition
or "not in" if selected "Allow multiple values" option.

REQUIREMENTS
------------

  Views.

INSTALLATION
------------

 - Extract the files in your module directory (typically /modules).
 - Visit the modules page and enable the module.

CONFIGURATION
------------

The Views Negate module provides a "Not Equal" to condition for textfield
and list field in contextual filter. It works same as exclude option of
numeric type field.

A checkbox with "Negate" label is added under "MORE" fieldset. By selecing the
Negate option, it applies the "not equal" condition or "not in" if selected
"Allow multiple values" option.

MAINTAINERS
-----------
* Sumit Madan (sumitmadan) - https://www.drupal.org/u/sumitmadan
* Yogesh Pawar (yogeshmpawar) - https://www.drupal.org/u/yogeshmpawar
* Jaideep Singh Kandari (JayKandari) - https://www.drupal.org/u/jaykandari

This project has been sponsored by:

* QED42
  Visit http://www.qed42.com for more information.
